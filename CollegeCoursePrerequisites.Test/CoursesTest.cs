using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CollegeCoursePrerequisites.Test
{
    [TestClass]
    public class CoursesTest
    {
        [TestMethod]
        public void TSort_IsExpectOrder_True()
        {
            var disorderedStringOfCourses = new[] { "Introduction to Paper Airplanes: ", "Advanced Throwing Techniques: Introduction to Paper Airplanes", "History of Cubicle Siege Engines: Rubber Band Catapults 101", "Advanced Office Warfare: History of Cubicle Siege Engines", "Rubber Band Catapults 101: ", "Paper Jet Engines: Introduction to Paper Airplanes" };
            var expectStringOfCourses = new[] { "Introduction to Paper Airplanes, Rubber Band Catapults 101, Paper Jet Engines, Advanced Throwing Techniques, History of Cubicle Siege Engines, Advanced Office Warfare", "Introduction to Paper Airplanes, Rubber Band Catapults 101, Advanced Throwing Techniques, History of Cubicle Siege Engines, Paper Jet Engines, Advanced Office Warfare" };

            var corses = new HashSet<Course>();
            CourseHelper.ParseToCourses(disorderedStringOfCourses).TSortGroup(i => i.Dependencies).ToList().ForEach(x => corses.UnionWith(x));
            var result = string.Join(", ", corses.Select(x => x.Name));

            Assert.AreEqual(expectStringOfCourses[0].Length, result.Length);
            CollectionAssert.Contains(expectStringOfCourses, result);

        }

        [TestMethod]
        public void TSort_IsResultWithoutCyclicDependency_True()
        {
            var disorderedStringOfCourses = new[] { "Introduction to Paper Airplanes: ", "Advanced Throwing Techniques: Introduction to Paper Airplanes", "History of Cubicle Siege Engines: Rubber Band Catapults 101", "Advanced Office Warfare: History of Cubicle Siege Engines", "Rubber Band Catapults 101: ", "Paper Jet Engines: Introduction to Paper Airplanes", "Intro to Arguing on the Internet: Godwin�s Law", "Understanding Circular Logic: Intro to Arguing on the Internet", "Godwin�s Law: Understanding Circular Logic" };
            var expectStringOfCourses = new[] { "Introduction to Paper Airplanes, Rubber Band Catapults 101, Paper Jet Engines, Advanced Throwing Techniques, History of Cubicle Siege Engines, Advanced Office Warfare", "Introduction to Paper Airplanes, Rubber Band Catapults 101, Advanced Throwing Techniques, History of Cubicle Siege Engines, Paper Jet Engines, Advanced Office Warfare" };

            var corses = new HashSet<Course>();
            CourseHelper.ParseToCourses(disorderedStringOfCourses).TSortGroup(i => i.Dependencies).ToList().ForEach(x => corses.UnionWith(x));
            var result = string.Join(", ", corses.Select(x => x.Name));

            Assert.AreEqual(expectStringOfCourses[0].Length, result.Length);
            CollectionAssert.Contains(expectStringOfCourses, result);
        }
    }
}
