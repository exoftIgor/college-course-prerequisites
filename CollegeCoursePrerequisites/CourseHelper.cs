﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollegeCoursePrerequisites
{
    public static class CourseHelper
    {
        public static IList<Course> ParseToCourses(string[] courseStr)
        {
            var result = new List<Course>();
            // set vertex
            foreach (var item in courseStr)
            {
                var spilitedItems = item.Split(": ");
                if (spilitedItems.Length < 2)
                    throw new Exception("Wrong a format of input string");
                result.Add(new Course(spilitedItems[0]));
            }
            // set edge
            foreach (var item in courseStr)
            {
                var spilitedItems = item.Split(": ");
                if (spilitedItems.Length < 2)
                    throw new Exception("Wrong a format of input string");
                var course = result.SingleOrDefault(x => x.Name == spilitedItems[0]);
                var excourse = result.SingleOrDefault(x => x.Name == spilitedItems[1]);
                if (excourse != null)
                    course.Dependencies.Add(excourse);
               
            }
            return result;
        }
    }
}
